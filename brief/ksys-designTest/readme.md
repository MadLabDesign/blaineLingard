**Instructions**

Turn the attached mockup (glassshot.jpg) into an HTML/CSS/JS single page web-app, adding as many personal touches as you like. Try to approximate the design, typography, colour as closely as possibe, recreating any assets where required. 

You can include animations, FX, the charting library (this does not need to be dynamic and fully functional, it can be purely representative) of your choice and any kind of interactivity.  The page should be full width and responsive.

You don't need to design anything other than what's in this mockup: what happens when clicking on any link to other pages can be verbally discussed or assumed. 

However, it does need to responsive but you can decided what that behaviour ought to be. Consider yourself to be the user, and define the experience that you would like have. It can be as rough or refined as you like and you can talk through any UI/UX decisions you may not be able to include.





