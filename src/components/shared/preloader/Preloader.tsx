import * as React from 'react';
import { PreloaderContainer } from './Preloader.style';

export interface IPreloaderProps {
}

export default class Preloader extends React.Component<IPreloaderProps, any> {
 render() {
    return (
      <PreloaderContainer>
      
      <div className="lines">
        <div className="line line-1"></div>
        <div className="line line-2"></div>
        <div className="line line-3"></div>
      </div>
  
        <div className="loading-text">LOADING</div>

     </PreloaderContainer>
    );
  }
}
