import * as React from 'react';
import { WarningPanelContainer, WarningHeader, WarningArticle, LabelStatus } from './WarningPanel.style';


const WarningList = [
  {
    articleLabel: "-24%",
    articleStatus: false,
    articleType: "PRO",
    articleTitle: "cheddar co",
    articleDesc: "Ut faucibus tristique neque, nec elementum libero dapibus non.",
    articleTicket: "ticket #1239-346",

  },{
    articleLabel: "+18%",
    articleStatus: true,
    articleType: "QA",
    articleTitle: "cheese limit",
    articleDesc: "Ut faucibus tristique neque, nec elementum libero dapibus non.",
    articleTicket: "ticket #1239-346",

  },{
    articleLabel: "+23%",
    articleStatus: false,
    articleType: "PRO",
    articleTitle: "edam plc",
    articleDesc: "Ut faucibus tristique neque, nec elementum libero dapibus non.",
    articleTicket: "ticket #1239-348",

  }
];

export interface IWarningPanelProps {
}

export default class WarningPanel extends React.Component<IWarningPanelProps, any> {
  public render() {
    return (
      <WarningPanelContainer>
        <WarningHeader>
          <span><i className="far fa-heart"/></span>  Warnings
        </WarningHeader>
{/* TODO: map throught articles */}

      {WarningList.map(({articleLabel, articleStatus, articleTitle, articleType, articleDesc, articleTicket}) => (
        <WarningArticle key={articleTitle}>
          <div className="article-header">
                <LabelStatus activeState={articleStatus}>{articleLabel}</LabelStatus>
                <h3>{articleTitle}</h3>
              <div className="article-type">{articleType}</div>
          </div>
          <div className="article-subHeader">
            <i className="fas fa-tag"/> {articleTicket}
          </div>
          <div className="article-desc">{articleDesc}</div>
        </WarningArticle>
      ))}
        
      </WarningPanelContainer>
    );
  }
}
