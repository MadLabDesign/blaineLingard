import styled from 'styled-components';
import { Colors } from '../../../../src/lib/theme/Colors';
import { dimensions } from '../../../lib/theme/variables';

export const WarningPanelContainer = styled.section`
    width: 100%;
    background-color: ${Colors.secondaryBG};
    display: block;
    padding: ${dimensions.padding.regular}rem 0;

    @media screen and (min-width: 768px){
        width: 100%;
    min-height:100%;
    background-color: ${Colors.secondaryBG};
		min-height:100vh;
		width: 400px;
        display: block;
        padding: ${dimensions.padding.large}rem 0;
    }
`;

export const WarningHeader = styled.div`
    text-align: center;
    color: ${Colors.text};
    text-transform: uppercase;
    letter-spacing: 0.2rem;
    padding-bottom: ${dimensions.padding.regular}rem;
`;

export const WarningArticle = styled.article`
    border-bottom: 1px solid ${Colors.heroBG};
    padding: ${dimensions.padding.regular}rem;
    .article-header {
        .label {
            letter-spacing: 0.2rem;
            padding: 0.1rem 0.5rem;
            height: 30px;
            width: 75px;
            margin-right: 0.8rem;
            border-radius: 4px;
            background-color: ${Colors.success};
            color: ${Colors.white};
            text-align: center;
            font-weight: 700;
            position: relative;
            display: inline-block;
            &.success {
                background-color: ${Colors.success};
            }
            &.danger {
                background-color: ${Colors.danger};
            }
        }
        h3 {
            font-size: 15px;
            text-transform: uppercase;
            letter-spacing: 0.1rem;
            color: ${Colors.titleText};
            display: inline-block;
            padding: 0;
            margin:0;
        }
        .article-type {
            font-size: 12px;
            right: 0;
            position: relative;
            text-transform: uppercase;
            float: right;
            text-align: right;
            color: ${Colors.text};
        }

    }
    .article-subHeader{
        text-transform: uppercase;
        height: 40px;
        line-height: 40px;
    }
    .article-desc{
        height: 60px;
        overflow: hidden;
    }

`;

interface ILabelProps {
	activeState?: boolean;
}

export const LabelStatus = styled.div<ILabelProps>`
    letter-spacing: 0.2rem;
    padding: 0.1rem 0.5rem;
    height: 30px;
    width: 75px;
    margin-right: 0.8rem;
    border-radius: 4px;
    background-color: ${props => (props.activeState ? Colors.success : Colors.danger)};
    color: ${Colors.white};
    text-align: center;
    font-weight: 700;
    position: relative;
    display: inline-block;
`;