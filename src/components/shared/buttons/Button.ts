import styled from 'styled-components';
import { Colors } from '../../../../src/lib/theme/Colors';
import { Link } from 'react-router-dom';

interface IButtonProps {
    active?: boolean;
  }

export const GetStarted = styled(Link)`
    min-width: 150px;
    max-width: 300px;
    padding: 0.8rem;
    height: 50px;
    display: block;
    text-align: center;
    background: ${Colors.madlabPrimary};
    position: relative;
    color: ${Colors.white};
    z-index: 20;
    text-decoration: none;
    top: 28%;
    border: none;
    border-radius: 2px;
    transition: all 2s ease-in;
   
    &:hover{
       
        background: ${Colors.white};
        color: ${Colors.madlabPrimary};
    }
`;

export const Button = styled(Link)<IButtonProps>`
    padding: 0 0.2rem;
    min-width: 90px;
    margin: 0 0.2rem;
    height: 35px;
    line-height: 30px;
    display: inline-block;
    text-align: center;
    background: transparent;
    position: relative;
    color: ${(props) => (props.active ? Colors.titleText : Colors.buttonText)};
    text-decoration: none;
    text-transform:uppercase;
    border: ${(props) => (props.active ?  "1px solid #666B88" : "1px solid transparent")};;
    border-radius: 5px;
    font-weight: 600;
    letter-spacing: 0.1rem;
    font-size: 12px;
    &:hover{
        background: transparent;
        color: ${Colors.titleText};
        border: 1px solid ${Colors.border} ;
    }

    @media screen and (min-width: 768px){
        display: inline-block;
        margin: 0 0.8rem;
        font-size: 14px;
        padding: 0 0.8rem;
        letter-spacing: 0.1rem;
        
    }
`;