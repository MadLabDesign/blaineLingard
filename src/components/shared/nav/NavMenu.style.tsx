import styled from 'styled-components';
import { Colors } from './../../../../src/lib/theme/Colors';

export const NavContainer = styled.div`
.nav-icon {
  margin: 0.6rem auto ;
  width: 30px;
  cursor: pointer;

}
.nav-icon div, .nav-icon:after, .nav-icon:before {
  content: "";
  background: ${Colors.primary};
  -moz-border-radius: 3px;
  -webkit-border-radius: 3px;
  border-radius: 3px;
  height: 3px;
  display: block;
  margin: 8px 0;
  -moz-transition: all 0.2s ease-in-out;
  -o-transition: all 0.2s ease-in-out;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}
.nav-icon:hover:before {
  -moz-transform: translateY(12px) rotate(135deg);
  -ms-transform: translateY(12px) rotate(135deg);
  -webkit-transform: translateY(12px) rotate(135deg);
  transform: translateY(12px) rotate(135deg);
}
.nav-icon:hover:after {
  -moz-transform: translateY(-12px) rotate(-135deg);
  -ms-transform: translateY(-12px) rotate(-135deg);
  -webkit-transform: translateY(-12px) rotate(-135deg);
  transform: translateY(-12px) rotate(-135deg);
}
.nav-icon:hover div {
  -moz-transform: scale(0);
  -ms-transform: scale(0);
  -webkit-transform: scale(0);
  transform: scale(0);
}

width: 50px;
    display: flex;
    justify-content: flex-end;
    height: 60px;
    @media screen  and (min-width: 768px) {
        display: none;
    }
`;