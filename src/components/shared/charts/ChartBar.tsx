import * as React from "react";
import {Bar} from 'react-chartjs-2';
import { ChartContainer } from './Chart.style';


const data = {
  responsive: true,
  labels: ['', '', '', '',  '', '', '', '', ''],
  datasets: [
    {
    
     label: 'Release',
     padding: 0,
      backgroundColor: '#727BC0',
      borderWidth: 0,
      hoverBackgroundColor: '#DAE0F6',

      data: [ 
        50, 
        40, 
        100, 
        50, 
        80, 
        140, 
        70,
        60,
        30,
        0,
        150
      ],
      scaleLabel: {
        display: false
      }
    }
  ]
 
};


export default class ChartBar extends React.Component{
  
    public render() {
      return (
        <ChartContainer>
        <Bar
            data={data}
            width={60}
            height={30}
            options={{
              responsive: true,
		          showInLegend: false, 
              animationEnabled: true,
             
              legend: {
                display: false
              },
              title: {
                display: false
              },
              scales: {
                xAxes: [{
                  barPercentage: 0.5,
                  barThickness: 55,
                  maxBarThickness:100,
                  gridLines: {
                    display: false,
                    offsetGridLines:false
                }
                }],
                yAxes: [{
                  display: false,
                  stacked: false,
                  scaleLabel: {
                    display: false
                  },
                  gridLines: {
                    display: false,
                    offsetGridLines:false
                }
              }]
              }
             
            }}
          />
        </ChartContainer>
      );
    }
}