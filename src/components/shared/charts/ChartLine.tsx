import * as React from "react";
import {Line} from 'react-chartjs-2';





const data = {
  labels: ['', '', '', '',  '', '', '', '', ''],
  datasets: [
    {
     label: '',
     padding: 10,
     borderColor: '#727BC0',
     pointBorderWidth: 8,
     lineTension: 0,
      backgroundColor: 'rgba(77,79,101,0.8)',
      borderWidth: 3,
      lineHeight: 2,
      hoverBackgroundColor: '#DAE0F6',
      data: [ 
        50, 
        40, 
        100, 
        50, 
        80, 
        140, 
        70,
        60,
        30,
        0,
        150,
      ],
      scaleLabel: {
        display: true
      }
    }
  ]
};


export default class ChartLine extends React.Component{
  
    public render() {
      return (
        <div className="chart">
        <Line
       
            data={data}
            width={60}
            height={25}
            options={{
              
              type: 'line', 
		          showInLegend: false, 
              animationEnabled: true,
              // maintainAspectRatio: true,
              lineTension: 0,
              legend: {
                display: false,
              },
              title: {
                // display: false
              },
              scales: {
                xAxes: [{
                  barPercentage: 0,
                  barThickness: 70,
                 
                  maxBarThickness:80,
                  gridLines: {
                      // display: false,
                      offsetGridLines:true,
                  }
                }],
                yAxes: [{
                    display: false,
                  stacked: false,
                  scaleLabel: {
                    display: false
                  },
                  gridLines: {
                    display: false,
                    offsetGridLines:true
                }
              }]
              }
             
            }}
          />
        </div>
      );
    }
}