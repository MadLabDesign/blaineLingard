import styled from 'styled-components';

export const ChartContainer  = styled.div`
    height: auto;
    position: relative;

    .chartjs-render-monitor {
        max-height: 100% !important;
        position: relative;
        
    }

    @media screen and (min-width: 768px){
        height: 260px !important;
        .chartjs-render-monitor {
            position: absolute;
            bottom: -13px;
        }
    }
`;