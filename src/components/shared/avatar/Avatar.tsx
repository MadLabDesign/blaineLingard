import * as React from 'react';
import { AvatarContainer, AvatarImage } from './Avatar.style';


export default class Avatar extends React.Component {
  public render() {
    return (
        <AvatarContainer>
            <AvatarImage>
            <img src="http://gravatar.com/avatar/953d4a723bed85b720fe14eba8084b43?s=80" alt="" />
            </AvatarImage>
            <div className="hide-mobile">blaine lingard</div>
      </AvatarContainer>
    );
  }
}
