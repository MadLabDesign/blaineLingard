import styled from "styled-components";
import { Colors } from "../../../lib/theme/Colors";


interface IAvatarProps {
  fullWidth?: boolean;
}

export const AvatarContainer = styled.div`
height: 60px;
 min-width: 35px;
    margin-top: 0.45rem;
    @media screen and (min-width: 768px){
      text-transform: uppercase;
    display: flex;
    align-items:center;
    justify-content: flex-start;
    min-width: 200px;
    padding-right: 0.4rem;
    letter-spacing: 0.1rem;
    font-size:16px;
    margin: 0;
    height: 60px;
    }
   
`;

export const AvatarImage = styled.div<IAvatarProps>`
margin: 0.2em;
position: relative;
display: inline-block;
vertical-align: -webkit-baseline-middle;

&:after {
  content: '';
  position: absolute;
  right: -.3rem;
  bottom: 0rem;
  width: 0.8rem;
  height: 0.8rem;
  border-radius: 60%;
  border: 6px solid ${Colors.navBG};
}

img {
  border-radius: 60%;
  width: 35px;
  height: 35px;
}

@media screen and (min-width: 768px){
  margin: 0em 0.8em;
}
`;