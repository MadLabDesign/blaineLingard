import styled from 'styled-components';
import { Colors } from '../../../../src/lib/theme/Colors';
import { dimensions } from '../../../lib/theme/variables';

interface ItemTab {
	activeState?: boolean;
}
export const MenuContainer = styled.div`
	width: 100%;
	
	display: flex;
	background-color: ${Colors.secondaryBG};
	float: left;
	position: relative;
	left: 0;
	text-align: center;

	@media screen and (max-width: 767px){
		height:40px;
	}
	@media screen and (min-width: 768px){
		min-height:100vh;
		width: 100px;
		display: block;
    }

	
`;

export const Item = styled.div<ItemTab>`
	padding: ${dimensions.padding.regular}rem;
	height: 40px;
	line-height: 30px;
	width: 100%;
	display: flex;
	align-items: center;
	justify-content: center;
	 cursor: pointer;
	 font-size: 20px;
	color: ${props => (props.activeState ? Colors.titleText : Colors.primary)};
	border-bottom: 1px solid;
	border-color: ${Colors.heroBG};
	background-color: ${Colors.secondaryBG};

	&:hover {
		border-color: ${Colors.titleText};
		color: ${Colors.titleText};
		border-bottom: 1px solid ${Colors.titleText};
		background-color: ${Colors.navBG};
	}

	@media screen and (min-width: 768px){
		width: 100px;
		height: 120px;
		line-height: 60px;
		font-size: 35px;
		border-bottom: ${props => (props.activeState ? '4px solid' : '1px solid')};
		background-color: ${props => (props.activeState ? Colors.navBG : Colors.secondaryBG)};

    }
`;