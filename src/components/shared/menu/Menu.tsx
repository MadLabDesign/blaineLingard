import * as React from 'react';
import { MenuContainer, Item } from './Menu.style';

export interface IMenuProps {
}

export default class Menu extends React.Component<IMenuProps, any> {
  render() {
    return (
      <MenuContainer>
        <Item activeState={true}>
        <span className="icon-gauge"/>
          </Item>
        <Item>
        <span className="icon-time"/>
        </Item>
        <Item>
        <span className="icon-graph"/>
        </Item>
      </MenuContainer>
    );
  }
}
