import * as React from 'react';
import { CardContainer, CardFooter, CardBody, CardMain, LeftCol, RightCol, CardHeader, CardMainRight, CardGraph} from './Card.style';
import { Button } from '../buttons/Button';
import ChartBar from '../charts/ChartBar';
const ButtonList = [
  {
    ButtonText:  "1 Week",
    ButtonTitle: "1 Week",
    ButtonLink: "#",
    ButtonStatus: true
  },
  {
    ButtonText:  "1 Month",
    ButtonTitle: "1 Month",
    ButtonLink: "#"
  },
  {
    ButtonText:  "3 Month",
    ButtonTitle: "3 Month",
    ButtonLink: "#"
  }
];
export interface ICardProps {
  cardTitle?: string;
  cardTitleRight?: string;
  cardSubTitle?: string;
  cardMain?: JSX.Element | string;
  cardMainRight?: JSX.Element | string;
}

export default class CardFull extends React.Component<ICardProps, any> {
  public render() {
    const {cardTitle, cardSubTitle, cardTitleRight, cardMain, cardMainRight} = this.props;
    return (
      <CardContainer fullWidth={true}>
        <CardBody>
            <LeftCol>
              <CardHeader>
                  <h2>
                    {cardTitle} <br />
                    <small>{cardSubTitle}</small>
                  </h2>
                </CardHeader>
              <CardMain> 
               {cardMain}
              </CardMain>
              <CardGraph fullHeight={true}> 
              <ChartBar/>
              </CardGraph>
              <CardFooter>
                  {ButtonList.map(({ButtonText, ButtonTitle, ButtonLink, ButtonStatus }) => (
                  <React.Fragment key={ButtonText}>
                    <Button active={ButtonStatus} to={ButtonLink} title={ButtonTitle}>{ButtonText}</Button>
                  </React.Fragment>
                ))}
              </CardFooter>
            </LeftCol>

        <RightCol>
        <CardHeader>
                  <h2>
                    {cardTitleRight}
                  </h2>
                </CardHeader>
              <CardMainRight> 
                  {cardMainRight}
              </CardMainRight>
              <CardFooter>
              <Button to="/"  active={true}>LOAD MORE</Button>
              </CardFooter>
        
        </RightCol>
        </CardBody>
      </CardContainer>
    );
  }
}