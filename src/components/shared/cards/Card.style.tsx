import styled from "styled-components";
import { Colors } from "../../../lib/theme/Colors";
import { dimensions } from "../../../lib/theme/variables";

interface ICardProps {
  fullWidth?: boolean;
  fullHeight?: boolean;
}

export const CardContainer = styled.div<ICardProps>`
  position: relative;
  display: block;
  width: 100%;
 

  @media screen and (min-width: 1140px){
    width: ${(props) => (props.fullWidth ? "100%" : "50%")};
    max-width: ${(props) => (props.fullWidth ? "100%" : "900px")};
    min-width: ${(props) => (props.fullWidth ? "100%" : "300px")};
    }
`;

export const CardBody = styled.div`
  background: ${Colors.cardBG};
  margin: 0.8rem;

  @media screen and (min-width: 1450px){
    margin: 1rem;
    }
`;

export const CardMain = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  min-height: 140px;
  padding:0 ${dimensions.padding.small}rem ${dimensions.padding.small}rem;
flex-direction: column;
  @media screen and (min-width: 1000px){
    padding: ${dimensions.padding.small}rem ${dimensions.padding.large}rem;
    flex-direction: row;
    height: 140px;
    }
`;

export const CardMainRight = styled.div`
  display: block;
  height: 380px;
  @media screen and (min-width: 1450px){
    padding: ${dimensions.padding.small}rem ${dimensions.padding.large}rem;
    }
`;

export const CardGraph = styled.div<ICardProps>`
  background-color: #46475B;
  height: 170px;
  overflow: hidden;
  @media screen and (max-width: 767px){
    height: ${(props) => (props.fullHeight ? "140px" : "90px")};
    }
  @media screen and (min-width: 1140px){
    height: ${(props) => (props.fullHeight ? "240px" : "175px")};
    }
  @media screen and (min-width: 1700px){
    max-height: 230px;
  }
`;

export const CardHeader = styled.div`
  height: 50px;
  padding: 1rem 0.8rem;
  h2 {
    font-weight: 600;
    padding: 0;
    margin: 0;
    color: ${Colors.titleText};
    letter-spacing: 0.1rem;
    line-height: 1.3;
    font-size: 16px;
    text-transform: uppercase;
    small {
      font-size: 13px;
      color: #858ca8;
      font-weight: 400;
      letter-spacing: 0;
      text-transform: none;
    }
  }
  @media screen and (min-width: 1450px){
    height: 80px;
    padding: 40px 30px 0;
    }
`;

export const CardFooter = styled.div`
  background: ${Colors.cardFooter};
  height: 3.75rem;
  padding: 0 0.8rem;
  display: flex;
  position: relative;
  align-items: center;
  bottom: 0;
  width: 100%;
  justify-content: space-around;

  @media screen and (min-width: 1450px){
    padding: 0 3rem;
    }
`;

export const DataNumber = styled.div`
    color: ${Colors.white};
    font-size: 80px;
    font-weight: 600;
    width: 100%;
    text-align: center;
    height: 115px;
    @media screen and (min-width: 1000px){
      width: 45%;
      height: auto;
    }
`;

export const DataDesc = styled.div`
    letter-spacing: 0.1rem;
    width: 100%;
    text-align: center;
    color: ${Colors.text};
    .data-desc-title {
      text-transform: uppercase;
     width: 100%;
    }
    .data-arrow {
      color: red;
      margin-right: 0.4rem;
      width: 0; 
      height: 0; 
      border-left: 7px solid transparent;
      border-right: 7px solid transparent;
      border-bottom: 12px solid ${Colors.success }  ;
      position: relative;
      top: -17px;
    }
    .date-values {
      color: ${Colors.titleText};
      font-size: 20px;
      font-weight: 300;
      display: flex;
      align-items: center;
      justify-content: space-between;
      max-width: 140px;
      margin: 0 auto;
    }

    @media screen and (min-width: 1000px){
      width: 40%;
      text-align: left;
      .date-values {
        margin: 0 0;
      }
    }
`;

export const LeftCol = styled.div`
  width: 100%;
  display: block;
  @media screen and (min-width: 1140px){
    width: 50%;
    display: inline-block;
    }
`;
export const RightCol = styled.div`
  width: 100%;
  background: #3e3f51;
  display: block;
  height: 100%;
  @media screen and (min-width: 1140px){
    width: 50%;
    display: inline-block;
    }
`;

export const DataList = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 45px;
  padding-left: 0.8rem;
  color: ${Colors.text};
  font-size: 14px;
  font-weight: 500;
  flex-wrap: nowrap;
  letter-spacing: 0.0.5rem;
  .amount {
    margin-right:0.8rem;
    font-weight: 600;
  }

  @media screen and (min-width: 1450px){
    font-size: 16px;
    }
`;


