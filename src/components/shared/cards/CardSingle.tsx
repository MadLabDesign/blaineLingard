import * as React from "react";

import {
  CardContainer,
  CardFooter,
  CardBody,
  CardMain,
  CardHeader,
  CardGraph
} from "./Card.style";
import { Button } from '../buttons/Button';
import ChartLine from '../charts/ChartLine';




const ButtonList = [
  {
    ButtonText:  "1 Week",
    ButtonTitle: "1 Week",
    ButtonLink: "#",
    ButtonStatus: true
  },
  {
    ButtonText:  "1 Month",
    ButtonTitle: "1 Month",
    ButtonLink: "#"
  },
  {
    ButtonText:  "3 Month",
    ButtonTitle: "3 Month",
    ButtonLink: "#"
  }
];
export interface ICardProps {
  cardTitle?: string;
  cardSubTitle?: string;
  cardMain?: JSX.Element | string;
}



export default class CardSingle extends React.Component<ICardProps, any> {
  public render() {
    const {cardTitle, cardSubTitle, cardMain} = this.props;
    return (
      <CardContainer>
        <CardBody>
          <CardHeader>
            <h2>
              {cardTitle} <br />
              <small>{cardSubTitle}</small>
            </h2>
          </CardHeader>
          <CardMain>{cardMain}</CardMain>
          <CardGraph>
           <ChartLine />
          </CardGraph>
          <CardFooter>
            {ButtonList.map(({ButtonText, ButtonTitle, ButtonLink, ButtonStatus }) => (
              <React.Fragment key={ButtonText}>
                <Button active={ButtonStatus} to={ButtonLink} title={ButtonTitle}>{ButtonText}</Button>
              </React.Fragment>
            ))}
          </CardFooter>
        </CardBody>
      </CardContainer>
    );
  }
}
