import styled from 'styled-components';
import { Colors } from '../../../../src/lib/theme/Colors';

export const NavBar = styled.nav`
    width: 100%;
    height: 50px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    background: ${Colors.navBG};
    line-height: 35px;
    color: ${Colors.primary};
    font-weight: 600;
    font-size: 16px;
    a {
        color: ${Colors.primary};
        text-decoration: none;
        padding: 1rem 0.8rem;
        line-height: 35px;
        font-size: 20px;

        @media screen and (min-width: 768px){
            font-size: 30px;
    }
    }
    @media screen and (min-width: 768px){
        height: 75px;
    }
    
`;

export const Brand = styled.div`
    font-weight: 700;
    width: 100px;
    height: 75px;
    background: ${Colors.primary};
    display: block;
    text-align: center;
    padding: 1.65rem 0.8rem 0;
    color: ${Colors.white};
    font-size: 23px;
`;

export const Tabs = styled.div`
    width: 80%;
    display: flex;
    justify-content: flex-end;
    height: 60px;

    @media screen  and  (max-width: 767px) {
        display: none;
    }
`;