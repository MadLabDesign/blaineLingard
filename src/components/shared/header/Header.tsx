import * as React from 'react';
import { NavBar, Brand, Tabs} from './Header.style';
import NavMenu from '../nav/Nav';

export interface IHeaderProps {
    linkDesc?: string;
    children?: JSX.Element | string;
}

export default class Header extends React.Component<IHeaderProps, any> {
  render() {
    const {children} = this.props;
    return (
      <NavBar>
         
        <>
        <Brand><span className="icon-glassLogo"/></Brand>
        <Tabs>
          {children}
        </Tabs>
        <NavMenu/>
       
        </>
      </NavBar>
    );
  }
}
