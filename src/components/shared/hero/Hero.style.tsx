import styled from 'styled-components';
import { Colors } from '../../../../src/lib/theme/Colors';
import { dimensions } from '../../../../src/lib/theme/variables';

export const HeroPanel = styled.section`
    background: ${Colors.heroBG};
    min-width: 100%;
    min-height: 255px;
    display: block;
    padding: ${dimensions.padding.regular}rem ${dimensions.padding.regular}rem  0;

    @media screen and (min-width: 768px){
        padding: ${dimensions.padding.large}rem ${dimensions.padding.large}rem  0;
    }
`;

export const HeroHeader = styled.div`
    display: flex;
    width: 100%;
    align-items: center;
    justify-content: space-between;
    color: ${Colors.titleText};
        h1 {
            font-weight: 600;
            padding: 0;
            margin: 0;
            text-transform: uppercase;
            font-size: 20px;
            letter-spacing: 0.2rem;
            }
        .close-tab {
            padding-top: 0.25rem;
            font-size: 25px;
            width: 35px;
            height: 35px;
            display: block;
            text-align: center;
            cursor: pointer;
            color: ${Colors.text};
            .fa-times-circle {
                transition:  0.2s ease-out;
            }
           
            &:hover {
                color: ${Colors.titleText};
                .fa-times-circle {
                    -webkit-transform: rotateZ(720deg);
      -moz-transform: rotateZ(720deg);
      transform: rotateZ(720deg);
                   
                }
              
      
            }
        }
`;
export const HeroMain = styled.p`
    color: ${Colors.text};
    padding-bottom: ${dimensions.padding.small}rem;
`;
export const HeroPanelFooter = styled.div`
    display: flex;
    position: relative;
    bottom: 0;
    color: ${Colors.titleText};
    border-top: 1px solid ${Colors.border};
    padding: ${dimensions.padding.small}rem 0;

    @media screen and (max-width:767px) {
        justify-content: space-between;
    }
    @media screen and (min-width: 768px){
        display: block;
        padding: ${dimensions.padding.regular}rem 0;
    }
`;