import * as React from 'react';
import { HeroPanel, HeroHeader, HeroMain, HeroPanelFooter } from './Hero.style';
import { Button } from '../buttons/Button';

export interface IHeroProps {
  title?: string;
  desc?: string;
}

export default class Hero extends React.Component<IHeroProps, any> {
  public render() {

    const {title, desc} = this.props;
    return (
      <HeroPanel>
        <HeroHeader>
        <h1>{title}</h1>
        <div className="close-tab">
          <i className="far fa-times-circle"/>
        </div>
        </HeroHeader>
        <HeroMain>
          {desc}
        </HeroMain>
        <HeroPanelFooter>
          <Button to={"/"} active={true}>MAY 2014</Button>
          <Button to={"/"}>Change Date</Button>
          </HeroPanelFooter>
      </HeroPanel>
    );
  }
}
