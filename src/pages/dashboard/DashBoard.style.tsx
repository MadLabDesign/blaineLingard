import styled from 'styled-components';
import { Colors } from '../../../src/lib/theme/Colors';

export const DashBoardLayout = styled.div`
    min-width: 100%;
    overflow: visible;
    display: flex;
    background: ${Colors.primaryBG};
    justify-content: space-between;
    flex-direction: column;

    @media screen and (min-width: 768px){
        flex-direction: row;
    }
`;

export const MainContainer = styled.section`
    display: block;
    width: 100%;
    overflow: scroll;
`;

export const Wrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
`;
