import * as React from 'react';
import Menu from '../../../src/components/shared/menu/Menu';
import { DashBoardLayout, MainContainer, Wrapper } from './DashBoard.style';
import WarningPanel from '../../../src/components/shared/warningPanel/WarningPanel';
import Hero from '../../../src/components/shared/hero/Hero';
import CardSingle from '../../components/shared/cards/CardSingle';
import CardFull from '../../../src/components/shared/cards/CardFull';
import { DataNumber, DataDesc, DataList } from '../../../src/components/shared/cards/Card.style';
import { LabelStatus } from '../../../src/components/shared/warningPanel/WarningPanel.style';

const MarketList = [
  {
    marketTitle:  "Cheddar Co",
    marketData: "2.43",
    marketLabel: "+11%",
    marketStatus: false
  },{
    marketTitle:  "Cheese Limited",
    marketData: "2.96",
    marketLabel: "+11%",
    marketStatus: false
  },{
    marketTitle:  "Edam PLC",
    marketData: "3.2",
    marketLabel: "+11%",
    marketStatus: false
  },{
    marketTitle:  "Brie & Sons",
    marketData: "3.1",
    marketLabel: "+11%",
    marketStatus: true
  },{
    marketTitle:  "Gorgonzola Inc",
    marketData: "3.2",
    marketLabel: "+11%",
    marketStatus: true
  },{
    marketTitle:  "Ricotta International",
    marketData: "2.6",
    marketLabel: "+11%",
    marketStatus: false
  },{
    marketTitle:  "Roquefort Inc",
    marketData: "1.9",
    marketLabel: "+11%",
    marketStatus: true
  }
];



export default class DashBoard extends React.Component<any> {
  public render() {
  
    return (
      <DashBoardLayout>
        
        <Menu/>
        <MainContainer>
          <Hero 
            title="Efficiency" 
            desc="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ullamcorper fermentum arcu, non suscipit mauris malesuada sed. Pellentesque maximus gravida aliquet. Quisque eros metus, euismod ac velit eget, consequat venenatis quam. Cras eleifend ipsum dui, lobortis malesuada lorem vulputate."/>
          <Wrapper>
            <CardSingle 
              cardTitle="Production" 
              cardSubTitle="Average number of FTE’s per client."
              cardMain={
                <>
                <DataNumber>0.82</DataNumber>
                <DataDesc>
                  <div className="data-desc-title">vs last month</div>
                  
                  <span className="date-values">
                    <div>
                      <span className="data-arrow"/>
                      0.04</div>
                    <div>0.78</div>
                  </span>

                </DataDesc>
                
                </>
              } 
              />

            <CardSingle 
              cardTitle="Client QA" 
              cardSubTitle="Average number of FTE’s used per client test cycle."
              cardMain={
                <>
                <DataNumber>2.09</DataNumber>
                <DataDesc>
                  <div className="data-desc-title">vs last month</div>
                  
                  <span className="date-values">
                    <div>
                      <span className="data-arrow"/>
                      0.04</div>
                    <div>0.78</div>
                  </span>
                </DataDesc>    
                </>
              } 
              />

            <CardFull 
              cardTitle="Release" 
              cardSubTitle="Average number of FTE’s per client."
              cardTitleRight="Release 8.3 avg"
              cardMain={
                <>
                <DataNumber>2.96</DataNumber>
                <DataDesc>
                  <div className="data-desc-title">vs last month</div>
                  
                  <span className="date-values">
                    <div>
                      <span className="data-arrow"/>
                      0.04</div>
                    <div>0.78</div>
                  </span>

                </DataDesc>
                
                </>
              } 
              cardMainRight={
              <>
              
              {MarketList.map(({marketTitle, marketData, marketLabel, marketStatus}) => (
                  <DataList key={marketTitle}>
                        <div>
                            {marketTitle} 
                        </div>
                      <div>
                        <span className="amount">{marketData} </span>
                        <LabelStatus activeState={marketStatus}>{marketLabel}</LabelStatus>
                      </div>
               
                  </DataList>
                ))}
              </>}
              />
          </Wrapper>
        </MainContainer>
        <WarningPanel/>
      </DashBoardLayout>
    );
  }
}
