import styled from 'styled-components';

export const HomeIntroContainer = styled.div`
.content {
    text-align: center;
    z-index: 1;
    display: flex;
    align-items: center;
    justify-items: center;
    flex-direction: column;
    flex-wrap: wrap;
    height: 100vh;
    overflow:hidden;
  }
  
  .background {
    perspective: 1000px;
  }
  
  
  
  .background__copy {
    opacity: 0.3;
  }
  
  .content__title {
    top: 25%;
    font-family: 'sabbath-black', serif;
    font-size: 14vw;
    font-weight: normal;
    mix-blend-mode: screen;
    line-height: 0.8;
    text-transform: lowercase;
    margin: 0;
    color: var(--color-title);
    z-index: 20;
    position: relative;
  }
  
  .content__tagline {
    top: 25%;
    color: var(--color-tagline);
    margin: 1rem 0 0 0;
    font-weight: bold;
    font-size: 1rem;
    letter-spacing: 0.125rem;
    word-spacing: 0.25rem;
    text-transform: uppercase;
    text-align: center;
    z-index: 20;
    position: relative;
  }
`;