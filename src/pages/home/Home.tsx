import * as React from 'react';
import '../../assets/styles/App.css';
import { GetStarted } from '../../../src/components/shared/buttons/Button';
import { HomeIntroContainer } from './Home.style';



class Home extends React.Component {
  render() {
    return (
      <HomeIntroContainer >
      <div className="content">
				<h2 className="content__title">MADLAB</h2>
				<p className="content__tagline">UX to Development and everything inbetween.</p>
     
          <GetStarted to="/dashboard">Get Started</GetStarted>
      
        <div className="background" />
			</div>
      </HomeIntroContainer >
    );
  }
}

export default Home;
