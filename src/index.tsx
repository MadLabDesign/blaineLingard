import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './index.css';

import { Route, Link, BrowserRouter as Router} from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';
import Home from './pages/home/Home';
import DashBoard from './pages/dashboard/DashBoarded';
import Header from './components/shared/header/Header';
import Avatar from './components/shared/avatar/Avatar';

ReactDOM.render(

  <Router>
    <>
    <Header>
   <>
   <Avatar/>
        {/* <Link to="/home">Home</Link> */}
        <Link to="/"> <span className="icon-settings"/></Link>
        <Link to="/dashboard"><span className="icon-alert"/></Link>
    </>
    </Header>
  <Route exact path="/" component={Home} />
  <Route path="/dashboard" component={DashBoard} />
</>

 </Router>
 ,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
